document.getElementById("toggle").addEventListener('click', function () {

    if (document.getElementById("toggle").checked) {
        document.getElementById('basicPrice').innerHTML = "$19.99";
        document.getElementById('professionalPrice').innerHTML = "$24.99";
        document.getElementById('masterPrice').innerHTML = "$39.99";
    }
    else {
        document.getElementById('basicPrice').innerHTML = "$199.99";
        document.getElementById('professionalPrice').innerHTML = "$249.99";
        document.getElementById('masterPrice').innerHTML = "$399.99";
    }
});